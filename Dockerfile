FROM openjdk:8

ADD ./target/hotel-service-*.jar /hotel-service.jar

EXPOSE 80

CMD java -jar hotel-service.jar
